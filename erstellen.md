![](OER-Portal-Beta-Banner.png)

# Erstellen

Eine kurze Zusammenfassung und Best Practices zum Erstellen von OER.

* Wie kann ich konkret mit OER beginnen?
* Wo erfahre ich mehr zu den einzelnen Themen?

## Formate

* Welche Formate werden unterstützt?
* Welche Formate sind besonders geeignet?
* Was ist Bronze, Silber und Gold bei den Formaten?

## Werkzeuge

* Welche Werkzeuge für welche Anforderungen?
* Was sind gute Open Source Werkzeuge?


Die Möglichkeit Ihre Materialien bearbeiten und Nachnutzen zu können, erfordert neben einem offenen Format auch die Erreichbarkeit der genutzten Tools und Programme.

Es gibt gute und bewährte Tools für unterschiedliche Medienformate (Video, Infografik, Audio, Abbildungen, Text), die als open source ausgewiesen sind.


## Didaktik

* Wie sollte ich meine Lernmaterialien didaktisch aufbereiten?
*   Welche Inhalte sind relevant? Wie wähle ich Inhalte aus?
*   Wie kann ich Inhalte strukturieren (segmentieren, sequenzieren)?
*   Wie kann ich Inhalte lernförderlich darstellen?
*   Wie kann ich Inhalte motivierend darstellen?


## Qualität

* Was ist gute Qualität bei OER?
* Worauf sollte ich bei der Erstellung achten?
