![](OER-Portal-Beta-Banner.png)

# Teilen

Eine kurze Zusammenfassung und Best Practices zum Erstellen von OER.

* Warum ist öffentlich nicht gleich sichtbar?
* Wie verbreite ich OER am besten?

## Lizenzieren

* Was kann ich bedenkenlos verwenden?
* Was sollte ich beim Mixen von OER beachten?
* Welche Lizenzen sollte ich bevorzugen?

## Metadaten vergeben

* Warum Metadaten?
* Wie halte ich mich an die richtigen Standards?

## Öffentlich bereitstellen

* Web, Cloud, GitLab/GitHub - wohin mit meinem OER?

## Auffindbar machen

* Warum ins Portal stellen, wenn es doch schon öffentlich verfügbar ist?
