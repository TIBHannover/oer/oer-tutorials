# Tutorials zum Erstellen und Teilen von OER

## OER Erstellen
* [Ebook](https://tibhannover.gitlab.io/oer/oer-tutorials/erstellen.epub)
* [PDF](https://tibhannover.gitlab.io/oer/oer-tutorials/erstellen.pdf)
* [HTML](https://tibhannover.gitlab.io/oer/oer-tutorials/erstellen.html)

## OER Teilen
* [Ebook](https://tibhannover.gitlab.io/oer/oer-tutorials/teilen.epub)
* [PDF](https://tibhannover.gitlab.io/oer/oer-tutorials/teilen.pdf)
* [HTML](https://tibhannover.gitlab.io/oer/oer-tutorials/teilen.html)
